using UnityEngine;

namespace Extensions
{
    public static class VectorExtension
    {
        public static float[] ToArray(this Vector2 v) => 
            new[] {v.x, v.y};
        
        public static int[] ToArray(this Vector3Int v) => 
            new[] {v.x, v.y};
    }
}