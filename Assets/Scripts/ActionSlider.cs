﻿using TMPro;
using UnityEngine;

public class ActionSlider : MonoBehaviour
{
    public RectTransform negatvieBar;
    public RectTransform positiveBar;
    public TMP_Text _text;

    public bool isVertical;
    
    private void Start()
    {
        UpdateBard(0f);
    }

    private void UpdateBard(float t)
    {
        // 0 = -1
        // 0.5 = 0
        var negativePos = (1 - Mathf.Clamp01(-t)) / 2;
        // 0.5 = 0
        // 1 = 1
        var positivePos = Mathf.Clamp01(t) / 2 + 0.5f;

        if (isVertical)
        {
            negatvieBar.anchorMin = new Vector2(negatvieBar.anchorMin.x, negativePos);
            positiveBar.anchorMax = new Vector2(positiveBar.anchorMax.x, positivePos);
        }
        else
        {
            negatvieBar.anchorMin = new Vector2(negativePos, negatvieBar.anchorMin.y);
            positiveBar.anchorMax = new Vector2(positivePos, positiveBar.anchorMax.y);
        }

        _text.text = t.ToString("F");
    }

    public float Value
    {
        set => UpdateBard(value);
    }
}
