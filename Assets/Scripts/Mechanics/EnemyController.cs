﻿using System.Collections;
using System.Collections.Generic;
using MLAgents;
using Platformer.Core;
using Platformer.Gameplay;
using Platformer.Model;
using UnityEngine;
using Zenject;
using static Platformer.Core.Simulation;

namespace Platformer.Mechanics
{
    /// <summary>
    /// A simple controller for enemies. Provides movement control over a patrol path.
    /// </summary>
    [RequireComponent(typeof(AnimationController), typeof(Collider2D))]
    public class EnemyController : MonoBehaviour
    {
        public PatrolPath path;
        public AudioClip ouch;
        public bool reversDeathBehaviour = false;

        internal PatrolPath.Mover mover;
        internal AnimationController control;
        internal Collider2D _collider;
        internal AudioSource _audio;
        internal Rigidbody2D _rigidbody;
          
        SpriteRenderer spriteRenderer;

        private Vector3 startPos;

        public Bounds Bounds => _collider.bounds;
        
        [Inject] private  PlatformerModel model = default;
        [Inject] private Simulation _simulation = default;

        void Awake()
        {
            control = GetComponent<AnimationController>();
            _collider = GetComponent<Collider2D>();
            _audio = GetComponent<AudioSource>();
            spriteRenderer = GetComponent<SpriteRenderer>();
            _rigidbody = GetComponent<Rigidbody2D>();
            startPos = transform.position;

            model.gameController.OnReset += () =>
            {
                gameObject.SetActive(true);
                transform.position = startPos;
                control.enabled = true;
                _collider.enabled = true;
                _rigidbody.velocity = Vector2.zero;
            };
        }

        protected virtual void OnCollisionEnter2D(Collision2D collision)
        {
            var player = collision.gameObject.GetComponent<PlayerController>();
            if (player != null)
            {
                var ev = _simulation.Schedule<PlayerEnemyCollision>();
                ev.player = player;
                ev.enemy = this;
            }
        }

        void Update()
        {
            if (path != null)
            {
                if (mover == null) mover = path.CreateMover(control.maxSpeed * 0.5f);
                control.move.x = Mathf.Clamp(mover.Position.x - transform.position.x, -1, 1);
            }
        }
    }
}