// using System;
// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using Platformer.Gameplay;
// using static Platformer.Core.Simulation;
// using Platformer.Model;
// using Platformer.Core;
// using Zenject;
//
// namespace Platformer.Mechanics
// {
//     /// <summary>
//     /// This is the main class used to implement control of the player.
//     /// It is a superset of the AnimationController class, but is inlined to allow for any kind of customisation.
//     /// </summary>
//     public class PlayerController : MonoBehaviour
//     {
//         /// <summary>
//         /// The minimum normal (dot product) considered suitable for the entity sit on.
//         /// </summary>
//         public float minGroundNormalY = .65f;
//         
//         /// <summary>
//         /// Max horizontal speed of the player.
//         /// </summary>
//         public float maxSpeed = 7;
//         /// <summary>
//         /// Initial jump velocity at the start of a jump.
//         /// </summary>
//         public float jumpTakeOffSpeed = 7;
//
//         public JumpState jumpState = JumpState.Grounded;
//         private bool stopJump;
//         /*internal new*/ public Collider2D collider2d;
//         public Health health;
//         public bool controlEnabled = true;
//
//         bool jump;
//         Vector2 move;
//         SpriteRenderer spriteRenderer;
//         internal Animator animator;
//         protected const float minMoveDistance = 0.001f;
//         private ContactFilter2D contactFilter;
//         private RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
//         private const float shellRadius = 0.01f;
//         private Vector2 velocity;
//         private Vector2 targetVelocity;
//
//         private Rigidbody2D rigidbody;
//         
//         [Inject]
//         private PlatformerModel model = default;
//         
//         [Inject] private Simulation _simulation = default;
//
//         public Bounds Bounds => collider2d.bounds;
//
//         public float HorizontalMovement { get; set; } = 0;
//
//         public bool PressJump { get; set; } = false;
//
//         /// <summary>
//         /// Is the entity currently sitting on a surface?
//         /// </summary>
//         /// <value></value>
//         public bool IsGrounded;
//         
//         void Awake()
//         {
//             health = GetComponent<Health>();
//             collider2d = GetComponent<Collider2D>();
//             spriteRenderer = GetComponent<SpriteRenderer>();
//             animator = GetComponent<Animator>();
//             rigidbody = GetComponent<Rigidbody2D>();
//         }
//
//         private void Start()
//         {
//             contactFilter.useTriggers = false;
//             // contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
//             contactFilter.useLayerMask = true;
//         }
//
//         protected void Update()
//         {
//             if (controlEnabled)
//             {
//                 move.x = HorizontalMovement;
//                 if (jumpState == JumpState.Grounded && PressJump)
//                     jumpState = JumpState.PrepareToJump;
//                 // else if (PressJump)
//                 // {
//                 //     stopJump = true;
//                 //     _simulation.Schedule<PlayerStopJump>().player = this;
//                 // }
//             }
//             else
//             {
//                 move.x = 0;
//             }
//             UpdateJumpState();
//         }
//
//         private void FixedUpdate()
//         {
//             PerformeGroundCheck(move);
//             ComputeVelocity();
//             rigidbody.velocity = targetVelocity;
//         }
//
//         void UpdateJumpState()
//         {
//             jump = false;
//             switch (jumpState)
//             {
//                 case JumpState.PrepareToJump:
//                     jumpState = JumpState.Jumping;
//                     jump = true;
//                     stopJump = false;
//                     break;
//                 case JumpState.Jumping:
//                     if (!IsGrounded)
//                     {
//                         jumpState = JumpState.InFlight;
//                     }
//                     break;
//                 case JumpState.InFlight:
//                     if (IsGrounded)
//                     {
//                         _simulation.Schedule<PlayerLanded>().player = this;
//                         jumpState = JumpState.Landed;
//                     }
//                     break;
//                 case JumpState.Landed:
//                     jumpState = JumpState.Grounded;
//                     break;
//             }
//         }
//
//         /// <summary>
//         /// Bounce the object's vertical velocity.
//         /// </summary>
//         /// <param name="value"></param>
//         public void Bounce(float value)
//         {
//             velocity.y = value;
//         }
//         
//         /// <summary>
//         /// Teleport to some position.
//         /// </summary>
//         /// <param name="position"></param>
//         public void Teleport(Vector3 position)
//         {
//             rigidbody.position = position;
//             velocity *= 0;
//             rigidbody.velocity *= 0;
//         }
//         
//         protected void ComputeVelocity()
//         {
//             if (jump && IsGrounded)
//             {
//                 velocity.y = jumpTakeOffSpeed * model.jumpModifier;
//                 jump = false;
//             }
//             else if (stopJump)
//             {
//                 stopJump = false;
//                 if (velocity.y > 0)
//                 {
//                     velocity.y = velocity.y * model.jumpDeceleration;
//                 }
//             }
//
//             if (move.x > 0.01f)
//                 spriteRenderer.flipX = false;
//             else if (move.x < -0.01f)
//                 spriteRenderer.flipX = true;
//
//             animator.SetBool("grounded", IsGrounded);
//             animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);
//
//             targetVelocity = move * maxSpeed;
//         }
//
//         public enum JumpState
//         {
//             Grounded,
//             PrepareToJump,
//             Jumping,
//             InFlight,
//             Landed
//         }
//         
//         void PerformeGroundCheck(Vector2 move)
//         {
//             var distance = move.magnitude;
//
//             if (distance > minMoveDistance)
//             {
//                 //check if we hit anything in current direction of travel
//                 var count = rigidbody.Cast(move + Vector2.down*0.1f, contactFilter, hitBuffer, distance + shellRadius);
//                 print(count);
//                 for (var i = 0; i < count; i++)
//                 {
//                     var currentNormal = hitBuffer[i].normal;
//
//                     //is this surface flat enough to land on?
//                     if (currentNormal.y > minGroundNormalY)
//                     {
//                         IsGrounded = true;
//                     }
//                 }
//             }
//         }
//     }
// }