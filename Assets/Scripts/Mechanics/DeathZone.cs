﻿using System.Linq;
using Platformer.Core;
using Platformer.Gameplay;
using UnityEngine;
using Zenject;

namespace Platformer.Mechanics
{
    /// <summary>
    /// DeathZone components mark a collider which will schedule a
    /// PlayerEnteredDeathZone event when the player enters the trigger.
    /// </summary>
    public class DeathZone : MonoBehaviour
    {
        [Inject]
        private Simulation _simulation = default;
        
        private void Start()
        {
            var collider = GetComponent<BoxCollider2D>();
            var (w, h) = ((int)collider.size.x, (int)collider.size.y);
            
            var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
            var tex = new Texture2D((int) w, (int) h);
            var colors = Enumerable.Range(0, w*h).Select(i => Color.red).ToArray();
            tex.SetPixels(colors);
            tex.Apply();
            var sprite = Sprite.Create(tex, new Rect(0, 0, w, h),
                new Vector2(0.5f, 0.5f), 1);
            spriteRenderer.sprite = sprite;
        }

        void OnTriggerEnter2D(Collider2D collider)
        {
            var p = collider.gameObject.GetComponent<PlayerController>();
            if (p != null)
            {
                var ev = _simulation.Schedule<PlayerEnteredDeathZone>();
                ev.deathzone = this;
            }
        }
    }
}