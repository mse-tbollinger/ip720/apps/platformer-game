using System;
using Platformer.Core;
using UnityEngine;
using Zenject;

namespace Platformer.Mechanics
{
    /// <summary>
    /// This class exposes the the game model in the inspector, and ticks the
    /// simulation.
    /// </summary>
    [DefaultExecutionOrder(-10)]
    public class GameController : MonoBehaviour
    {
        public bool GameVictory { get; set; } = false;

        public event Action OnReset;

        [Inject] private Simulation _simulation = default;

        void Update()
        {
            _simulation.Tick();
        }

        public void Reset()
        {
            GameVictory = false;
            OnReset?.Invoke();
        }
    }
}