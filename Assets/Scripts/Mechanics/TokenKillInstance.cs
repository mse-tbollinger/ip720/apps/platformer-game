using Platformer.Gameplay;

namespace Platformer.Mechanics
{
    public class TokenKillInstance : TokenInstance
    {
        protected override void OnPlayerEnter(PlayerController player)
        {
            base.OnPlayerEnter(player);
            _simulation.Schedule<PlayerDeath>();
        }
    }
}