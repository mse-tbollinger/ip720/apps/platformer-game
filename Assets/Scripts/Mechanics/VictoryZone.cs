using System.Linq;
using Platformer.Core;
using Platformer.Gameplay;
using UnityEngine;
using Zenject;
using static Platformer.Core.Simulation;

namespace Platformer.Mechanics
{
    /// <summary>
    /// Marks a trigger as a VictoryZone, usually used to end the current game level.
    /// </summary>
    public class VictoryZone : MonoBehaviour
    {
        [Inject] private Simulation _simulation = default;
        
        private void Start()
        {
            var collider = GetComponent<BoxCollider2D>();
            var (w, h) = ((int)collider.size.x, (int)collider.size.y);
            
            var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
            var tex = new Texture2D((int) w, (int) h);
            var colors = Enumerable.Range(0, w*h).Select(i => Color.blue).ToArray();
            tex.SetPixels(colors);
            tex.Apply();
            var sprite = Sprite.Create(tex, new Rect(0, 0, w, h),
                new Vector2(0.5f, 0.5f), 1);
            spriteRenderer.sprite = sprite;
        }
        
        void OnTriggerEnter2D(Collider2D collider)
        {
            var p = collider.gameObject.GetComponent<PlayerController>();
            if (p != null)
            {
                var ev = _simulation.Schedule<PlayerEnteredVictoryZone>();
                ev.victoryZone = this;
            }
        }
    }
}