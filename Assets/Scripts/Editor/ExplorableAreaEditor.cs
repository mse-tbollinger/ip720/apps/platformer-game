using System.Runtime.InteropServices;
using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(ExplorableArea))]
public class ExplorableAreaEditor : Editor
{
    public ExplorableArea Target => (ExplorableArea)target;
    
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Draw"))
        {
            Target.LoadFile();
            Target.Redraw();
        }
    }

    private bool _leftCtrlPressed = false;
    
    private void OnSceneGUI()
    {
        var e = Event.current;

        if (e.type == EventType.KeyDown && e.keyCode == KeyCode.LeftControl)
        {
            _leftCtrlPressed = true;
        }
        if (e.type == EventType.KeyUp && e.keyCode == KeyCode.LeftControl)
        {
            _leftCtrlPressed = false;
        }

        if (e.type == EventType.MouseDrag && e.button == 1)
        {
            var mousePos = e.mousePosition;
            var ray = HandleUtility.GUIPointToWorldRay(mousePos);
            if (_leftCtrlPressed)
            {
                Target.RemoveExplorable(ray.origin);
            }
            else
            {
                Target.AddExplorable(ray.origin);
            }
            
            e.Use();
        }
        
        if (e.type == EventType.MouseDown && e.button == 1)
        {
            var mousePos = e.mousePosition;
            var ray = HandleUtility.GUIPointToWorldRay(mousePos);
            if (_leftCtrlPressed)
            {
                Target.RemoveExplorable(ray.origin);
            }
            else
            {
                Target.AddExplorable(ray.origin);
            }
            e.Use();
        }
        
        if (e.type == EventType.KeyDown && e.keyCode == KeyCode.F5)
        {
            Target.SaveToFile();
            e.Use();
        }
        
        if (e.type == EventType.KeyDown && e.keyCode == KeyCode.F8)
        {
            Target.Redraw();
            e.Use();
        }

        // foreach (var vector in Target.Explorable)
        // {
        //     var cameraSize = Mathf.Abs(Target.MlCamera.ViewportToWorldPoint(Vector3.zero).x - Target.MlCamera.ViewportToWorldPoint(Vector3.one).x);
        //     
        //     var pos = Target.GridToWorldPos(vector, true) - new Vector3(cameraSize/2, cameraSize/2);
        //     // var size = new Vector2(0.4f, 0.4f);
        //     var size = new Vector2(cameraSize, cameraSize);
        //     var rect = new Rect(pos, size);
        //     Handles.DrawSolidRectangleWithOutline(
        //         rect, Color.clear, new Color(0, 1, 0, 1f));
        // }
    }
}
