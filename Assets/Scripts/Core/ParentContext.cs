using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Zenject.Internal;

namespace Platformer.Core
{
    public class ParentContext : SceneContext
    {
        protected override void GetInjectableMonoBehaviours(List<MonoBehaviour> monoBehaviours)
        {
            ZenUtilInternal.GetInjectableMonoBehavioursUnderGameObject(gameObject, monoBehaviours);
        }
    }
}