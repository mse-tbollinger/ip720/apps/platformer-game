using Ml;
using Platformer.Core;
using Platformer.Model;
using UnityEngine;
using Zenject;

public class MlInstaller : MonoInstaller
{
    [SerializeField] private PlatformerModel platformerModel = default;
    
    public override void InstallBindings()
    {
        Container.Bind<PlatformerModel>().FromInstance(platformerModel);
        Container.Bind<Simulation>().FromInstance(new Simulation(platformerModel));
    }
}