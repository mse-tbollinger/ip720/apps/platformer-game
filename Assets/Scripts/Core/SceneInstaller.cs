using UnityEngine;
using Zenject;

namespace Ml
{
    public class SceneInstaller : MonoInstaller
    {
        public const string OriginSpawnPointId = "OriginSpawnPointId";

        public ActionSlider _action0Slider;
        public ActionSlider _action1Slider;

        private BugLocationCollector _bugLocationCollector = new BugLocationCollector();
        // private Transform _originSpawnPoint;

        public const string Action0Slider = "Action0Slider";
        public const string Action1Slider = "Action1Slider";
        
        public override void InstallBindings()
        {
            Container.Bind<BugLocationCollector>().FromInstance(_bugLocationCollector).AsSingle();
            Container.Bind<ActionSlider>().WithId(Action0Slider).FromInstance(_action0Slider);
            Container.Bind<ActionSlider>().WithId(Action1Slider).FromInstance(_action1Slider);
        }

        public void OnDestroy()
        {
            _bugLocationCollector.SaveToFile();
        }
    }
}