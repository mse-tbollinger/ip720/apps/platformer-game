using System;
using Ml;
using MLAgents.Policies;
using UnityEngine;

namespace Platformer.Core
{
    public class PlaygroundController : MonoBehaviour
    {
        [SerializeField] private PlaygroundArea[] playgrounds = default;

        private readonly KeyCode[] switchKeys = new KeyCode[]
        {
            KeyCode.Alpha1,
            KeyCode.Alpha2,
            KeyCode.Alpha3,
            KeyCode.Alpha4,
            KeyCode.Alpha5,
            KeyCode.Alpha6,
        };

        private readonly Rect[] cameraRects4 = new[]
        {
            new Rect(0, 0, .5f, .5f),
            new Rect(0.5f, 0, .5f, .5f),
            new Rect(0, 0.5f, .5f, .5f),
            new Rect(0.5f, 0.5f, .5f, .5f),
        };
        
        private readonly Rect[] cameraRects6 = new[]
        {
            new Rect(0, 0, .33f, .5f),
            new Rect(0.33f, 0, .33f, .5f),
            new Rect(0.66f, 0, .33f, .5f),
            new Rect(0, 0.5f, .33f, .5f),
            new Rect(0.33f, 0.5f, .33f, .5f),
            new Rect(0.66f, 0.5f, .33f, .5f),
        };

        private void Start()
        {
            ShowAll();
        }

        private void Update()
        {
            for (int i = 0; i < playgrounds.Length; i++)
            {
                if (Input.GetKeyDown(switchKeys[i]))
                {
                    foreach (var playground in playgrounds)
                    {
                        playground.mainCamera.rect = Rect.zero;
                        playground.platformerAgent.ControlsEnabled = false;
                    }
                    
                    playgrounds[i].mainCamera.rect = new Rect(0, 0, 1, 1);
                    playgrounds[i].platformerAgent.ControlsEnabled = true;
                }
            }

            if (Input.GetKeyDown(KeyCode.Alpha0))
            {
                ShowAll();
            }
        }

        private void ShowAll()
        {
            if(playgrounds.Length == 1) return;
            if (playgrounds.Length <= 4)
            {
                for (int i = 0; i < 4; i++)
                {
                    playgrounds[i].platformerAgent.ControlsEnabled = true;
                    playgrounds[i].mainCamera.gameObject.SetActive(true);
                    playgrounds[i].mainCamera.rect = cameraRects4[i];
                }
            }
            else
            {
                for (int i = 0; i < 6; i++)
                {
                    playgrounds[i].platformerAgent.ControlsEnabled = true;
                    playgrounds[i].mainCamera.gameObject.SetActive(true);
                    playgrounds[i].mainCamera.rect = cameraRects6[i];
                }
            }
        }
    }
}