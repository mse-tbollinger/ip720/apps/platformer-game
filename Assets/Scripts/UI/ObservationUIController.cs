using UnityEngine;

namespace Platformer.UI
{
    public class ObservationUIController : MonoBehaviour
    {
        private GameObject _child;
        
        private void Start()
        {
            _child = transform.GetChild(0).gameObject;
            _child.SetActive(false);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.H))
            {
                _child.SetActive(!_child.activeSelf);
            }
        }
    }
}