using MLAgents.Policies;
using Platformer.Mechanics;
using UnityEngine;

namespace Ml
{
    public class PlaygroundArea : MonoBehaviour
    {
        [SerializeField] private Rect _area;
        public Camera mainCamera;
        public Camera mlCamera;
        public PlatformerAgent platformerAgent;
    }
}