using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

namespace Ml
{
    public class BugLocationCollector
    {
        class Data
        {
            public float X { get;  }
            public float Y { get;  }
            public float Radius { get;  }
            public string Name { get;  }
            
            public int InstanceId { get; }

            public Data(Vector2 location, float radius, string name, int instanceId)
            {
                X = location.x;
                Y = location.y;
                Radius = radius;
                Name = name;
                InstanceId = instanceId;
            }
        }
        
        private readonly List<Data> _bugLocation = new List<Data>();
        private const string FileName = "bug_locations.json";
        private bool _alreadySaved;

        public void Log(Vector2 location, float radius, string name, int instanceId)
        {
            _bugLocation.Add(new Data(location, radius, name, instanceId));
        }

        public void SaveToFile()
        {
            if(_alreadySaved) return;
            _alreadySaved = true;
            var json = JsonConvert.SerializeObject(_bugLocation);
            var path = Application.dataPath + "/Data/" + FileName;
            File.WriteAllText(path, json);
            Debug.Log($"File written to {path}");
        }
    }
}