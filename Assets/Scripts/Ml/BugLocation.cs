using Platformer.Model;
using UnityEngine;
using Zenject;

namespace Ml
{
    public class BugLocation : MonoBehaviour
    {
        [Inject] private BugLocationCollector _bugLocationCollector;
        [Inject] private PlatformerModel _model;
        
        [SerializeField] private float radius = 0.5f;
        [SerializeField] private string bugName;
        
        public void Start()
        {
            if (!Application.isEditor)
            {
                Destroy(this);
                return;
            }

            var pos = _model.TransformToOrigin(transform.position);
            _bugLocationCollector.Log(pos, radius, bugName, _model.instanceId);
        }

        public void OnDrawGizmos()
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawSphere(transform.position, radius);
        }
    }
}