using System;
using Extensions;
using MLAgents.SideChannels;
using UnityEngine;

namespace Ml
{
    public class PlatformerSideChannel : SideChannel
    {
        public PlatformerSideChannel()
        {
            ChannelId = new Guid("14e56707-e8ad-4e25-b60b-ea1fb4c878f0");
        }
        
        public override void OnMessageReceived(IncomingMessage msg)
        {
            throw new NotImplementedException();
        }

        public void SendMessage(int instance, Vector2 pos, Vector3Int gridPos)
        {
            using (var msgOut = new OutgoingMessage())    
            {
                msgOut.WriteInt32(instance);
                msgOut.WriteFloatList(pos.ToArray());
                msgOut.WriteInt32(gridPos.x);
                msgOut.WriteInt32(gridPos.y);
                QueueMessageToSend(msgOut);
            }
        }
    }
}