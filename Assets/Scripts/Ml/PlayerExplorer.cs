using UnityEngine;

namespace Ml
{
    public class PlayerExplorer : MonoBehaviour
    {
        [SerializeField] private ExplorableArea explorableArea = default;

        private void Start()
        {
            if (!Application.isEditor || explorableArea == null)
            {
                Destroy(this);
            }
        }

        private void Update()
        {
            explorableArea.AddExplorable(transform.position);
        }
    }
}