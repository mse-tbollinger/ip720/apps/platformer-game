using System;
using MLAgents;
using Platformer.Core;
using Platformer.Gameplay;
using Platformer.Mechanics;
using Platformer.Model;
using TMPro;
using UnityEngine;
using Zenject;

namespace Ml
{
    public class PlatformerAgent : Agent
    {
        private PlayerController playerController;
        
        [Inject]
        private PlatformerModel model = default;
        
        [Inject] 
        private Simulation _simulation = default;

        [Inject(Id = SceneInstaller.Action0Slider)]
        private ActionSlider _actionSlider0;
        
        [Inject(Id = SceneInstaller.Action1Slider)]
        private ActionSlider _actionSlider1;
        
        private float startDist;

        private float rewardAccum;

        private int _stayStillCounter;
        private float _posAccumulator;
        private int _posCounter;

        private static PlatformerSideChannel _platformerSideChannel;

        private Transform _transform;

        private int instanceId;
        [SerializeField] private bool mirrorHeuristic = default;
        [SerializeField] private bool useLogger;
        [SerializeField] private TMP_Text _rewardDebugText;
        
        public bool ControlsEnabled = true;

        private static bool _first = true;

        private Vector3 _offset;
        
        private void Start()
        {
            _transform = transform;
            instanceId = model.instanceId;

            if (instanceId == -1)
            {
                throw new ArgumentException("Instance Id needs to be set in the Ml Installer");
            }
            
            if (_first && useLogger)
            {
                _platformerSideChannel = new PlatformerSideChannel();
                Academy.Instance.RegisterSideChannel(_platformerSideChannel);
                _first = false;
            }
            
            playerController = GetComponent<PlayerController>();
            startDist = Vector3.Distance(model.spawnPoint.position, model.victoryZone.transform.position);
            _offset = model.spawnPoint.position - model.originSpawnPoint.position;
        }
        
        public override void OnEpisodeBegin()
        {
            _simulation.Schedule<PlayerSpawn>(0);
        }
        
        public override void OnActionReceived(float[] vectorAction)
        {
            var action0 =  Mathf.Clamp(vectorAction[0], -1f, 1f);
            var action1 = vectorAction[1];
            
            if(_actionSlider0 != null)
                _actionSlider0.Value = action0;
            
            if(_actionSlider0 != null)
                _actionSlider1.Value = action1;
            
            playerController.HorizontalMovement = action0;
            playerController.PressJump = action1 > 0f;

            var dist = Vector3.Distance(model.victoryZone.transform.position, transform.position);
            // var reward = (transform.position.x - _lastPos.x) * 5;
            var reward = (1 - dist / startDist) * 2;
            
            if (useLogger)
            {
                var pos = _transform.position - _offset;
                _platformerSideChannel.SendMessage(instanceId, pos, ExplorableArea.WorldToGridPos(_transform.position, model.explorableGrid, model.explorableOffset));
            }
            
            if (model.gameController.GameVictory)
            {
                rewardAccum += 1;
            }
            if (!playerController.health.IsAlive)
            {
                rewardAccum -= 1;
            }

            SetReward(reward + rewardAccum);
            if (_rewardDebugText != null)
            {
                _rewardDebugText.text = (reward + rewardAccum).ToString();
            }
            rewardAccum = 0;
            
            if (model.gameController.GameVictory || !playerController.health.IsAlive)
            {
                EndEpisode();
            }
        }
    
        public override float[] Heuristic()
        {
            var action = new float[2];
            if (ControlsEnabled)
            {
                action[0] = Input.GetAxis("Horizontal") * (mirrorHeuristic ? -1 : 1);
                action[1] = Convert.ToSingle(Input.GetButton("Jump")) * 2 - 1;
            }
            return action;
        }

        public void RegisterEvent(PlatformerEvent e)
        {
            switch (e)
            {
                case PlatformerEvent.TokenCollected:
                    rewardAccum += 0.5f;
                    break;
            }
        }

        public void OnDestroy()
        {
            if (Academy.IsInitialized)
            {
                Academy.Instance.UnregisterSideChannel(_platformerSideChannel);
            }
        }
    }
}