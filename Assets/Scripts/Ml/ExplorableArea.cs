﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using TMPro;
using UnityEngine;
using UnityEngine.Tilemaps;
using File = System.IO.File;

[ExecuteInEditMode]
public class ExplorableArea : MonoBehaviour
{
    [SerializeField] private Transform _player = default;
    [SerializeField] private Tilemap _targetTilemap = default;
    [SerializeField] private Tilemap _srcTilemap = default;

    [SerializeField] private int _tileJumpHeight = 9;
    [SerializeField] private Tile notWalkableTile = default;
    [SerializeField] private Tile walkableTile = default;

    [SerializeField] private TextAsset _exploredFile = null;

    [SerializeField] private Camera _mlCamera = default;
    [SerializeField] private TMP_Text _debugText;
    [SerializeField] private string _fileName = "explorable.json";
    
    private readonly Dictionary<Vector3Int, float> _jumpPoints = new Dictionary<Vector3Int, float>();
    private readonly HashSet<Vector3Int> _visited = new HashSet<Vector3Int>();
    private readonly Queue<Vector3Int> _girdQueue = new Queue<Vector3Int>();
    private HashSet<Vector3Int> _explorable = new HashSet<Vector3Int>();
    private Dictionary<Vector3Int, TMP_Text> _debugTexts = new Dictionary<Vector3Int, TMP_Text>();

    public IReadOnlyCollection<Vector3Int> Explorable => _explorable;
    public Camera MlCamera => _mlCamera;

    private float _radius;

    private float upPenalty = 1f;
    private float horizontalPenalty = 0.25f;
    private float verticalUpPentalty = 0.5f;
    private float leftPenalty = 0.25f;
    
    void Start()
    {
        if (!Application.isEditor)
        {
            Destroy(_targetTilemap.gameObject);
            Destroy(gameObject);
        }
        
        _radius = _srcTilemap.layoutGrid.cellSize.x / 2f;

        if (_exploredFile == null && Application.isPlaying)
        {
            StartCoroutine(StartGridExplore(_player.position));
        }
        else
        {
            LoadFile();
            Draw();
        }
    }

    public void LoadFile()
    {
        if(_exploredFile != null)
            _explorable = JsonConvert.DeserializeObject<HashSet<Vector3Int>>(_exploredFile.text);
    }
    
    private void Draw()
    {
        foreach (var cord in _explorable)
        {
            _targetTilemap.SetTile(cord, walkableTile);
        }
    }

    public void AddExplorable(Vector3 worldPos)
    {
        var cords = WorldToGridPos(worldPos);
        if (!_explorable.Contains(cords))
        {
            _explorable.Add(cords);
            _targetTilemap.SetTile(cords, walkableTile);
        }
    }
    
    public void RemoveExplorable(Vector3 worldPos)
    {
        var cords = WorldToGridPos(worldPos);
        if (_explorable.Contains(cords))
        {
            _explorable.Remove(cords);
            _targetTilemap.SetTile(cords, null);
        }
    }

    public void Redraw()
    {
        _targetTilemap.ClearAllTiles();
        Draw();
    }

    private Vector3Int WorldToGridPos(Vector3 worldPos)
    {
        return WorldToGridPos(worldPos, _targetTilemap);
    }

    public static Vector3Int WorldToGridPos(Vector3 worldPos, Tilemap tilemap)
    {
        var cords = tilemap.layoutGrid.WorldToCell(worldPos - tilemap.transform.position);
        cords.z = 0;
        return cords;
    }
    
    public static Vector3Int WorldToGridPos(Vector3 worldPos, Grid grid, Vector3 offset)
    {
        var cords = grid.WorldToCell(worldPos - offset);
        cords.z = 0;
        return cords;
    }

    public Vector3 GridToWorldPos(Vector3Int gridPos, bool center = false)
    {
        var leftBottom = _targetTilemap.layoutGrid.CellToWorld(gridPos) + _targetTilemap.transform.position;

        if (!center) return leftBottom;
        var rightTop = _targetTilemap.layoutGrid.CellToWorld(gridPos + Vector3Int.one) + _targetTilemap.transform.position;
        rightTop.z = 0;
        return Vector3.Lerp(leftBottom, rightTop, 0.5f);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F5))
        {
            SaveToFile();
        }

        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            AddExplorable(Input.mousePosition);
        }
    }

    private IEnumerator StartGridExplore(Vector2 pos)
    {
        var cords = WorldToGridPos(pos);

        while (!HasCollider(cords))
        {
            cords += Vector3Int.down;
        }

        UpdateTile(cords + Vector3Int.up, _tileJumpHeight - 1);
        UpdateNeighbours(cords, _tileJumpHeight);
        _visited.Add(cords);
        _girdQueue.Enqueue(cords + Vector3Int.up);
        
        while (_girdQueue.Count > 0)
        {
            cords = _girdQueue.Dequeue();
            GridExplore(cords);
            if(_girdQueue.Count % 4 == 0)
                yield return null;
        }
        print("Done");
        SaveToFile();
    }

    public void SaveToFile()
    {
        var json = JsonConvert.SerializeObject(_explorable);
        var path = Application.dataPath + "/Data/" + _fileName;
        File.WriteAllText(path, json);
        print($"File written to {path}");
    }

    private void GridExplore(Vector3Int cords)
    {
        if (_visited.Contains(cords))
        {
            return;
        }
        _visited.Add(cords);
        
        var jumpPoints = Mathf.Max(GetMaxJumpPoints(cords), 0);

        if (HasCollider(cords))
        {
            jumpPoints = 0;
        }
        else if (HasCollider(cords + Vector3Int.down))
        {
            jumpPoints = _tileJumpHeight - 1;
        }

        UpdateTile(cords, jumpPoints);
        UpdateNeighbours(cords, jumpPoints);

        if (jumpPoints > 0 && jumpPoints < _tileJumpHeight)
        {
            _girdQueue.Enqueue(cords + Vector3Int.down);
            _girdQueue.Enqueue(cords + Vector3Int.down + Vector3Int.left);
            _girdQueue.Enqueue(cords + Vector3Int.down + Vector3Int.right);
            _girdQueue.Enqueue(cords + Vector3Int.up);
            _girdQueue.Enqueue(cords + Vector3Int.left);
            _girdQueue.Enqueue(cords + Vector3Int.right);
        }
    }

    private float GetMaxJumpPoints(Vector3Int cord)
    {
        if (HasCollider(cord))
        {
            return 0;
        }
        
        float GetHorizontal(Vector3Int cords)
        {
            var points = _jumpPoints.Get(cords);
            return points;
        }
        
        var max1 = Mathf.Max(_jumpPoints.Get(cord + Vector3Int.down) - upPenalty, _jumpPoints.Get(cord + Vector3Int.up));
        var max2 = Mathf.Max(
            GetHorizontal(cord + Vector3Int.left) - horizontalPenalty, 
            GetHorizontal(cord + Vector3Int.right) - leftPenalty);

        return Mathf.Max(max1, max2);
    }

    private void UpdateTile(Vector3Int cords, float jumpPoints)
    {
        if (HasCollider(cords))
        {
            jumpPoints =  0;
        }
        
        _jumpPoints[cords] = jumpPoints;
        if (jumpPoints <= 0 || jumpPoints >= _tileJumpHeight)
        {
            _targetTilemap.SetTile(cords, notWalkableTile);
            if (_explorable.Contains(cords))
            {
                _explorable.Remove(cords);
            }
        }
        else
        {
            _targetTilemap.SetTile(cords, walkableTile);
            _explorable.Add(cords);
        }

        var text = _debugTexts.GetOrCreate(cords, CreateDebugText);
        text.transform.position = GridToWorldPos(cords, true);
        text.text = jumpPoints.ToString();
    }

    private TMP_Text CreateDebugText()
    {
        var go = Instantiate(_debugText, transform);
        return go.GetComponent<TMP_Text>();
    }
    
    private void UpdateNeighbours(Vector3Int cord, float jumpPoints)
    {
        float Update(float x, float y, float minus)
        {
            if (y >= _tileJumpHeight)
            {
                return x;
            }
            return Mathf.Max(x, y - minus);
        }
        
        UpdateTile(cord + Vector3Int.right, Update(_jumpPoints.Get(cord + Vector3Int.right), jumpPoints, horizontalPenalty));
        UpdateTile(cord + Vector3Int.left , Update(_jumpPoints.Get(cord + Vector3Int.left ), jumpPoints, leftPenalty));
        UpdateTile(cord + Vector3Int.up   , Update(_jumpPoints.Get(cord + Vector3Int.up   ), jumpPoints, upPenalty));
        UpdateTile(cord + Vector3Int.down , Update(_jumpPoints.Get(cord + Vector3Int.down ), jumpPoints, 0));
    }

    private bool HasCollider(Vector3Int cords)
    {
        var worldPos = GridToWorldPos(cords, true);
        if (Physics2D.OverlapCircleAll(worldPos, _radius).Any(collider => collider.CompareTag("NotExplorable")))
        {
            return true;
        }
        
        var colliderType = _srcTilemap.GetColliderType(cords);
        return colliderType != Tile.ColliderType.None;
    }
}

static class DictExtension
{
    public static T Get<T>(this Dictionary<Vector3Int, T> dict, Vector3Int cords)
    {
        return dict.TryGetValue(cords, out var value) ? value : default(T);
    }

    public static T GetOrCreate<T>(this Dictionary<Vector3Int, T> dict, Vector3Int key, Func<T> factory)
    {
        if (dict.ContainsKey(key))
        {
            return dict[key];
        }
        else
        {
            var value = factory();
            dict.Add(key, value);
            return value;
        }
    }
}
