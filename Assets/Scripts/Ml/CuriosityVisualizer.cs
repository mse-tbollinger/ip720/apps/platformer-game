﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[ExecuteInEditMode]
public class CuriosityVisualizer : MonoBehaviour
{
    [SerializeField] private TextAsset _dataCsv = default;

    private const string XColumn = "x";
    private const string YColumn = "y";
    private const string CuriosityColumn = "curiosity";

    private int _xColIdx = -1;
    private int _yColIdx = -1;
    private int _curiosityIdx = -1;

    private float _minCuriosity = float.MaxValue;
    private float _maxCuriosity = float.MinValue;
    
    private List<(Vector2 position, float curiosity)> _data = new List<(Vector2 position, float curiosity)>();
    private int _csvHash = 0;
    

    private void Update()
    {
        if(_dataCsv == null && _dataCsv.GetHashCode() == _csvHash) return;
        _csvHash =_dataCsv.GetHashCode();
        
        var lines = _dataCsv.text.Split(System.Environment.NewLine[0]);
        var head = lines.First().Split(',');

        for (var i = 0; i < head.Length; i++)
        {
            var col = head[i].Trim().ToLower();
            
            switch (col)
            {
                case XColumn:
                    _xColIdx = i;
                    break;
                case YColumn:
                    _yColIdx = i;
                    break;
                case CuriosityColumn:
                    _curiosityIdx = i;
                    break;
            }
        }

        foreach (var line in lines.Skip(1))
        {
            var cols = line.Split(',');
            if(cols.Length == 1) continue;

            var x = float.Parse(cols[_xColIdx]);
            var y = float.Parse(cols[_yColIdx]);
            var curiosity = float.Parse(cols[_curiosityIdx]);

            _minCuriosity = Mathf.Min(curiosity, _minCuriosity);
            _maxCuriosity = Mathf.Max(curiosity, _maxCuriosity);
            
            _data.Add((new Vector2(x, y), curiosity));
        }
    }
    
    private void OnDrawGizmosSelected()
    {
        if (_data.Count <= 0) return;
        
        foreach (var record in _data)
        {
            var a = (record.curiosity - _minCuriosity) / _maxCuriosity;
            Gizmos.color = new Color(1, 0, 0, a);
            Gizmos.DrawSphere(new Vector3(record.position.x, record.position.y, 1), a);
            // Gizmos.DrawWireCube(new Vector3(record.position.x, record.position.y, 1), new Vector3(0.1f, 0.1f, (record.curiosity - _minCuriosity)/_maxCuriosity * 1.5f));
        }
    }
}
