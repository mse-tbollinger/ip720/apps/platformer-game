using MLAgents;
using UnityEngine;

namespace Ml
{
    public class SideChannelLogger : MonoBehaviour
    {
        LoggerSideChannel _loggerSideChannel;
        
        public void Awake()
        {
            _loggerSideChannel = new LoggerSideChannel(LogType.Log);
            
            Application.logMessageReceived += _loggerSideChannel.Log;
            Academy.Instance.RegisterSideChannel(_loggerSideChannel);
        }

        public void OnDestroy()
        {
            Application.logMessageReceived -= _loggerSideChannel.Log;
            if (Academy.IsInitialized){
                Academy.Instance.UnregisterSideChannel(_loggerSideChannel);
            }
        }
    }
}