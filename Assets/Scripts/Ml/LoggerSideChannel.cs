using System;
using MLAgents.SideChannels;
using UnityEngine;

namespace Ml
{
    public class LoggerSideChannel : SideChannel
    {
        private readonly LogType _logLevel;
        
        public LoggerSideChannel(LogType logLevel)
        {
            _logLevel = logLevel;
            ChannelId = new Guid("ebd81812-4f51-46ea-af8f-e6fa1965640a");
        }

        public override void OnMessageReceived(IncomingMessage msg)
        {
            throw new NotImplementedException();
        }

        public void Log(string msg, string stackTrace, LogType type)
        {
            if (_logLevel > type) return;
            
            var stringToSend = $"{type}: {msg} \n {stackTrace}";
            using (var msgOut = new OutgoingMessage())
            {
                msgOut.WriteString(stringToSend);
                QueueMessageToSend(msgOut);
            }
        }
    }
}