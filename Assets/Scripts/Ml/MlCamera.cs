﻿using UnityEngine;

public class MlCamera : MonoBehaviour
{
    private Camera _camera;
    // Start is called before the first frame update
    void Start()
    {
        _camera = GetComponent<Camera>();

        var min = _camera.ViewportToWorldPoint(Vector3.zero);
        var max = _camera.ViewportToWorldPoint(new Vector3(1, 1, 0));

        var dist = max - min;
        var w = dist.x;
        var h = dist.y;
        
        print($"Camera width: {w}");
        print($"Camera height: {h}");
    }
}
