using System;
using UnityEngine;

namespace Ml
{
    public class ScreenShotCamera : MonoBehaviour
    {
        private Camera _camera;
        [SerializeField] private int _instanceId;
        [SerializeField] private string _prefix = "TestLevel";
        
        private void Start()
        {
            _camera = GetComponent<Camera>();
        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.F1))
            {
                ScreenCapture.CaptureScreenshot($"{_prefix}{_instanceId}.png");

                var bottomLeft = _camera.ViewportToWorldPoint(new Vector3(0, 0, 0));
                var topRight = _camera.ViewportToWorldPoint(new Vector3(1, 1, 0));
                
                print(bottomLeft);
                print(topRight);
            }
        }
    }
}