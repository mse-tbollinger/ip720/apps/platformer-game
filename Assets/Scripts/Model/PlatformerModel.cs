using Ml;
using Platformer.Mechanics;
using UnityEngine;

namespace Platformer.Model
{
    /// <summary>
    /// The main model containing needed data to implement a platformer style 
    /// game. This class should only contain data, and methods that operate 
    /// on the data. It is initialised with data in the GameController class.
    /// </summary>
    [System.Serializable]
    public class PlatformerModel
    {
        /// <summary>
        /// The virtual camera in the scene.
        /// </summary>
        public Cinemachine.CinemachineVirtualCamera virtualCamera;

        /// <summary>
        /// The main component which controls the player sprite, controlled 
        /// by the user.
        /// </summary>
        public PlayerController player;

        /// <summary>
        /// The spawn point in the scene.
        /// </summary>
        public Transform spawnPoint;

        /// <summary>
        /// A global jump modifier applied to all initial jump velocities.
        /// </summary>
        public float jumpModifier = 1.5f;

        /// <summary>
        /// A global jump modifier applied to slow down an active jump when 
        /// the user releases the jump input.
        /// </summary>
        public float jumpDeceleration = 0.5f;

        public VictoryZone victoryZone;

        public PlatformerAgent platformerAgent;

        public GameController gameController;

        public Grid explorableGrid;
        public Vector3 explorableOffset;

        public Transform originSpawnPoint;

        public int instanceId = -1;
        
        private Vector3? _offset;

        private Vector3 Offset => 
            _offset ?? (_offset = spawnPoint.position - originSpawnPoint.position).Value;
        
        public Vector3 TransformToOrigin(Vector3 pos)
        {
            return pos - Offset;
        }
    }
}