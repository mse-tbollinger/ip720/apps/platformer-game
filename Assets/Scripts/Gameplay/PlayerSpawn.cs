using Platformer.Core;
using Platformer.Mechanics;
using Platformer.Model;
using Zenject;

namespace Platformer.Gameplay
{
    /// <summary>
    /// Fired when the player is spawned after dying.
    /// </summary>
    public class PlayerSpawn : Simulation.Event<PlayerSpawn>
    {
        public override void Execute()
        {
            var player = Model.player;
            player.collider2d.enabled = true;
            // player.controlEnabled = false;
            player.health.Increment();
            player.Teleport(Model.spawnPoint.transform.position);
            player.jumpState = PlayerController.JumpState.Grounded;
            player.animator.SetBool("dead", false);
            Model.virtualCamera.m_Follow = player.transform;
            Model.virtualCamera.m_LookAt = player.transform;
            Schedule<EnablePlayerInput>(0f);
            Model.gameController.Reset();
        }
    }
}