using Platformer.Core;
using Platformer.Mechanics;
using Platformer.Model;
using Zenject;

namespace Platformer.Gameplay
{

    /// <summary>
    /// This event is triggered when the player character enters a trigger with a VictoryZone component.
    /// </summary>
    /// <typeparam name="PlayerEnteredVictoryZone"></typeparam>
    public class PlayerEnteredVictoryZone : Simulation.Event<PlayerEnteredVictoryZone>
    {
        public VictoryZone victoryZone;

        public override void Execute()
        {
            // model.player.animator.SetTrigger("victory");
            // Model.player.controlEnabled = false;
            Model.gameController.GameVictory = true;
        }
    }
}