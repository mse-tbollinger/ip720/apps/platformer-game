using Platformer.Core;
using Platformer.Mechanics;
using Platformer.Model;
using Zenject;

namespace Platformer.Gameplay
{
    /// <summary>
    /// Fired when a player enters a trigger with a DeathZone component.
    /// </summary>
    /// <typeparam name="PlayerEnteredDeathZone"></typeparam>
    public class PlayerEnteredDeathZone : Simulation.Event<PlayerEnteredDeathZone>
    {
        public DeathZone deathzone;
        
        public override void Execute()
        {
            Schedule<PlayerDeath>(0);
        }
    }
}