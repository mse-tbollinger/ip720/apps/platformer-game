using Platformer.Core;
using Platformer.Model;
using Zenject;

namespace Platformer.Gameplay
{
    /// <summary>
    /// This event is fired when user input should be enabled.
    /// </summary>
    public class EnablePlayerInput : Simulation.Event<EnablePlayerInput>
    {
        public override void Execute()
        {
            var player = Model.player;
        }
    }
}