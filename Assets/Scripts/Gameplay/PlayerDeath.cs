﻿using System.Collections;
using System.Collections.Generic;
using Platformer.Core;
using Platformer.Model;
using UnityEngine;
using Zenject;

namespace Platformer.Gameplay
{
    /// <summary>
    /// Fired when the player has died.
    /// </summary>
    /// <typeparam name="PlayerDeath"></typeparam>
    public class PlayerDeath : Simulation.Event<PlayerDeath>
    {
        public override void Execute()
        {
            var player = Model.player;
            if (player.health.IsAlive)
            {
                player.health.Die();
                Model.virtualCamera.m_Follow = null;
                Model.virtualCamera.m_LookAt = null;
                // player.collider.enabled = false;
                
                player.animator.SetTrigger("hurt");
                player.animator.SetBool("dead", true);
                //Simulation.Schedule<PlayerSpawn>(2);
            }
        }
    }
}